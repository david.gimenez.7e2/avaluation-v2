import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AusenciasPage } from './ausencias.page';

const routes: Routes = [
  {
    path: '',
    component: AusenciasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AusenciasPageRoutingModule {}
