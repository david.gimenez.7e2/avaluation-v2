import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ausencias',
  templateUrl: './ausencias.page.html',
  styleUrls: ['./ausencias.page.scss'],
})
export class AusenciasPage implements OnInit {

  valor:number = 0;

  constructor() { }

  ngOnInit() {
  }

  cambiarValor(value){

    if (value==3){

      value = 0;

    }else{

      value ++;

    }

    this.valor = value;    

  }

}
