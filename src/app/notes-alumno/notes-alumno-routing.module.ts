import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotesAlumnoPage } from './notes-alumno.page';

const routes: Routes = [
  {
    path: '',
    component: NotesAlumnoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotesAlumnoPageRoutingModule {}
