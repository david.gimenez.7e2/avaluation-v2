import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotesAlumnoPageRoutingModule } from './notes-alumno-routing.module';

import { NotesAlumnoPage } from './notes-alumno.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotesAlumnoPageRoutingModule
  ],
  declarations: [NotesAlumnoPage]
})
export class NotesAlumnoPageModule {}
