import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiserviceService } from '../services/apiservice.service';

@Component({
  selector: 'app-notes-alumno',
  templateUrl: './notes-alumno.page.html',
  styleUrls: ['./notes-alumno.page.scss'],
})
export class NotesAlumnoPage implements OnInit {

  nomModul:any;
  user:any;
  tasques:any[];
  notes:any[]=[];

  constructor(private route: ActivatedRoute, private router: Router, private apiService: ApiserviceService) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.nomModul = this.router.getCurrentNavigation().extras.state.modulo.nombre_modulo;
        this.user = this.router.getCurrentNavigation().extras.state.user;
      }
    });
  }

  async ngOnInit() {
    this.tasques = await this.apiService.getNotasAlumno({dni: this.user.dni, nombre_modulo:this.nomModul});
    if (this.tasques.length > 0){
      this.tasques.forEach( (tasca) => {
        this.notes.push(tasca.notas.split(","));
      });
    }
    console.log(this.notes);
  }

  setBackgroundColor(nota){
    if (nota >= 5){
      return{
        'background-color':'green'
      }
    }
    if (nota < 5){
      return{
        'background-color':'red'
      }
    }
  }

}
