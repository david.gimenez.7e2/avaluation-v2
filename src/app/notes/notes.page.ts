import { StylesCompileDependency } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ApiserviceService } from '../services/apiservice.service';
import { ExcelService } from '../services/excel-service.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.page.html',
  styleUrls: ['./notes.page.scss'],
})
export class NotesPage implements OnInit {

  nomModul:any;
  user:any;
  dnis:any[]=[];
  notes:any[]=[];
  alumnes:any[];
  tasques:any[];
  notasExcel:any[]=[];

  constructor(private route: ActivatedRoute, private router: Router, private apiService: ApiserviceService, private excelService: ExcelService) {
    this.excelService = excelService;
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.nomModul = this.router.getCurrentNavigation().extras.state.modulo.nombre_modulo;
        this.user = this.router.getCurrentNavigation().extras.state.user;
      }
    });
  }

  private _csvData: any = null;
  private _fileName: any = "Alumnos";

  async ngOnInit() {
    this.tasques = await this.apiService.getNotas({nombre_modulo: this.nomModul});
    //Mira si tasques se ha rellenado o si está vacío
    var aux:any[] = await this.apiService.getAlumnoModulos({nombre_modulo: this.nomModul});
    aux.forEach( (dni) => {
      this.dnis.push(dni.dni_alumno);
    });
    if (this.tasques.length > 0){
      this.tasques.forEach( (tasca) => {
        this.notes.push(tasca.notas.split(","));
      });
    }
    console.log("---DNIs---");
    console.log(this.dnis);
    console.log("---Notes---");
    console.log(this.notes);
    console.log("---Tasques---");
    console.log(this.tasques);
    this.alumnes = await this.apiService.getAlumnosModulo({nombre_modulo: this.nomModul});
  }

  numToArray(ntasques): Array<number> {
    return Array(ntasques);
  }

  goToAddEjercicio(){
    let navigationExtras: NavigationExtras = {
      state: {
        nomModul: this.nomModul,
        dniprofe: this.user.dni,
        dnis: this.dnis
      }
    };
    this.router.navigate(['add-ejercicio'], navigationExtras);
  }

  async downloadExcel(event){

    console.log(this.notasExcel);

    this.excelService.exportAsExcelFile(this.notasExcel,'Alumnos');


  }

  pasoParametro(nota){
    console.log(nota);
    let navigationExtras: NavigationExtras = {
      state: {
        parametros: nota,
      }
    };
    this.router.navigate(['notes-detail'], navigationExtras);
  }

  setBackgroundColor(nota){
    if (nota >= 5){
      return{
        'background-color':'green'
      }
    }
    if (nota < 5){
      return{
        'background-color':'red'
      }
    }
  }

}