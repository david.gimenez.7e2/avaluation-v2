import { HttpClient, JsonpClientBackend } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {

  [x: string]: any;
  //url = 'http://avaluation.alwaysdata.net/miapi';
  url = 'http://localhost:3000/miapi';

  constructor() { }

       /**
  * Get the detailed information for an ID using the "i" parameter
  * 
  * @param {string} id imdbID to retrieve information
  * @returns Observable with detailed information
  */
        getDetails(id) {
          return this.http.get(`${this.url}?i=${id}&plot=full&apikey=${this.apiKey}`);
        }

  async getAlumnos() {
    const info = await (await fetch(`${this.url}/getAlumnos`)).json()
    return info
  }

<<<<<<< HEAD
  async getAlumnosModulo(modulo: any){
    const info = await (await fetch(`${this.url}/getAlumnosModulo`,{
=======
  async getDni(user: any) {
    const info = await (await fetch(`${this.url}/Login`,{
>>>>>>> 17ef2cea9b5ae55df4ed1135d854e22f5115164c
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      
      body: JSON.stringify(modulo)
    })).json()
    return info
  }

  async getDniProfesor(user: any) {
    const info = await (await fetch(`${this.url}/LoginProfesor`,{
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(user)
    })).json()
    return info
  }

  async getDniAlumno(user: any) {
    const info = await (await fetch(`${this.url}/LoginAlumno`,{
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(user)
    })).json()
    return info
  }

  async getModulos(user: any){
    const info = await (await fetch(`${this.url}/getModulos`,{
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(user)
    })).json()
    return info
  }

<<<<<<< HEAD
  async getModulosAumno(dni: any){
    console.log(dni)
    const info = await (await fetch(`${this.url}/getModulosAlumno`,{
=======
  async getModulos(user: any){
    console.log(user)
    const info = await (await fetch(`${this.url}/getModulos`,{
>>>>>>> 17ef2cea9b5ae55df4ed1135d854e22f5115164c
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
<<<<<<< HEAD
      body: JSON.stringify(dni)
    })).json()
    return info
  }

  async getAlumnoModulos(modulo: any){
    console.log(modulo)
    const info = await (await fetch(`${this.url}/postAlumnoModulo`,{
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(modulo)
    })).json()
    return info
  }

  async getCount(userModulo: any){
    console.log(userModulo)
    const info = await (await fetch(`${this.url}/getCount`,{
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(userModulo)
    })).json()
    return info
  }

  async setNotas(tarea: any){
    console.log(tarea)
    const info = await (await fetch(`${this.url}/setNotas`,{
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(tarea)
    }))
    return info
  }

  async getNotas(modulo: any){
    const info = await (await fetch(`${this.url}/getNotas`,{
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(modulo)
    })).json()
    return info
  }

  async getNotasAlumno(userModulo: any){
    const info = await (await fetch(`${this.url}/getNotasAlumno`,{
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(userModulo)
    })).json()
    return info
  }

  async getExcelNotas(modulo: any){
    const info = await (await fetch(`${this.url}/getExcelNotas`,{
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(modulo)
    })).json()
    return info
  }

  async getDetailedNotas(dni: any){
    console.log(dni)
    const info = await (await fetch(`${this.url}/getDetailedNotas`,{
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(dni)
    })).json()
    return info
  }

  async deleteNota(idNota) {
    console.log(idNota.nombre_entrega)
    const info=await fetch(`${this.url}/getNotas/${idNota.nombre_entrega}`,{
      method:"DELETE",
    })
    return info
  }

  async updateNota(idNota) {
    console.log(idNota.nombre_entrega)
    const info=await fetch(`${this.url}/getNotas/${idNota.nombre_entrega}`,{
      method:"POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(idNota)
    });
    return info
  }

  async getNotaAlumno(dni: any){
    console.log(dni)
    const info = await (await fetch(`${this.url}/getNotaAlumno`,{
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(dni)
=======
      body: JSON.stringify(user)
>>>>>>> 17ef2cea9b5ae55df4ed1135d854e22f5115164c
    })).json()
    return info
  }

}