const express = require("express"); // es el nostre servidor web
const jwt = require("jsonwebtoken");
const cors = require('cors'); // ens habilita el cors recordes el bicing???
const bodyParser = require('body-parser'); // per a poder rebre jsons en el body de la resposta
const app = express();
const baseUrl = '/miapi';
app.use(cors());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.use(bodyParser.urlencoded({ extended: false })); //per a poder rebre json en el reuest
app.use(bodyParser.json());

//La configuració de la meva bbdd

//El pool es un congunt de conexions
//const Pool = require('pg').Pool
//const pool = new Pool(ddbbConfig);

var mysql = require('mysql');
var connection = mysql.createConnection({
    user: '234185',
    host: 'mysql-avaluation.alwaysdata.net',
    database: 'avaluation_bbdd',
    password: 'ProjecteAvaluation',
    port: 3306
});

//Exemple endPoint
//Quan accedint a http://localhost:3000/miapi/test   ens saludará
const getAlumnos = (request, response) => {

<<<<<<< HEAD
    var consulta = "SELECT * FROM Alumno"

    connection.query(consulta, function (err, rows, fields) {
        if (err) throw err;
        response.status(200).json(rows)
    });

}

// GET ALUMNO MODULO --> Obtenemos todos los dnis de los alumnos que pertenecen a un modulo en especifico

const getAlumnoModulos = (request, response) => {

    console.log(request.body);
    const {nombre_modulo}=request.body;
    
    var consulta = `SELECT dni_alumno FROM AlumnoModulo WHERE nombre_modulo = '${nombre_modulo}' ORDER BY dni_alumno ASC`;

    connection.query(consulta, function (err, rows, fields) {
        if (err) throw err;
        response.status(200).json(rows)
    });

}

app.post(baseUrl + '/postAlumnoModulo', getAlumnoModulos);

//-----------------------------------------------------------------------------------------------------------

// GET ALL NOTAS --> NOTAS PARA EL EXCEL

const getExcelNotas = (request, response) => {
    
    const {nombre_modulo}=request.body;

    var consulta = `SELECT a.nombre_alumno, t.nombre_entrega, t.nombre_modulo, t.uf, t.nota FROM Tarea t, Alumno a WHERE t.dni_alumno = a.dni_alumno AND t.nombre_modulo = '${nombre_modulo}' ORDER BY a.nombre_alumno ASC, t.uf ASC `;
    
    console.log(consulta);
    connection.query(consulta, function (err, rows, fields) {
        if (err) throw err;
        response.status(200).json(rows)
    });
}
app.post(baseUrl + '/getExcelNotas', getExcelNotas);

//-----------------------------------------------------------------------------------------------------------

// GET AUSENCIAS

const getAusencias = (request, response) => {
    
    const {nombre_modulo}=request.body;

    var consulta = `SELECT m.nombre_alumno, a.nombre_modulo, a.faltas, a.justificadas, a.uf FROM Ausencias a, Alumno m WHERE m.dni_alumno = a.dni_alumno AND nombre_modulo = '${nombre_modulo}';`;
    
    console.log(consulta);
    connection.query(consulta, function (err, rows, fields) {
        if (err) throw err;
        response.status(200).json(rows)
    });
}
app.post(baseUrl + '/getAusencias', getAusencias);

//----------------------------------------------------------------------------------------------------------

// UPDATE AUSENCIAS

const updateAusencias = (request, response) => {
    console.log(request.body);
    console.log(request.params.nombre_modulo);

    var consulta = "UPDATE Ausencia set ? WHERE nombre_modulo = ?";

    connection.query(consulta, [request.body, request.params.nombre_modulo], (err, rows)=>{

        if(err) return response.send(err);
        response.send("Updated!");
    });
}

app.post(baseUrl + '/getNotas/:nombre_modulo', updateAusencias);

//----------------------------------------------------------------------------------------------------------

// GET NOTAS --> A partir del triple Inner Join obtenemos el nombre y las notas de los ejercicios de un almno que pertenece a un modulo en especifico

const getNotas = (request, response) => {
    
    const {nombre_modulo}=request.body;

    var consulta = `SELECT COUNT(*),dni_alumno, group_concat(CONCAT(nota)) as notas FROM Tarea WHERE Tarea.nombre_modulo = '${nombre_modulo}' GROUP by dni_alumno`;
    //var consulta = `SELECT Tarea.nota, Alumno.nombre_alumno, Alumno.dni_alumno, Tarea.nombre_modulo FROM Tarea, Alumno WHERE Tarea.dni_alumno = Alumno.dni_alumno`;
    console.log(consulta);
    connection.query(consulta, function (err, rows, fields) {
        if (err) throw err;
        response.status(200).json(rows)
    });

}

app.post(baseUrl + '/getNotas', getNotas);

//-----------------------------------------------------------------------------------------------------------

// GET NOTAS ALUMNO --> A partir del triple Inner Join obtenemos las notas de los ejercicios de un alumno que pertenece a un modulo en especifico

const getNotasAlumno = (request, response) => {
    
    const {dni, nombre_modulo}=request.body;

    var consulta = `SELECT COUNT(*),dni_alumno, group_concat(CONCAT(nota)) as notas FROM Tarea WHERE Tarea.nombre_modulo = '${nombre_modulo}' AND Tarea.dni_alumno = '${dni}' GROUP by dni_alumno`;
    connection.query(consulta, function (err, rows, fields) {
        if (err) throw err;
        response.status(200).json(rows)
    });

}

app.post(baseUrl + '/getNotasAlumno', getNotasAlumno);

//-----------------------------------------------------------------------------------------------------------

// GET COUNT --> Obtenemos el numero de ejercicios de un modulo para establecer sus columnas

const getCount = (request, response) => {

    const {dni, nombre_modulo}=request.body;

    var consulta = `SELECT COUNT(nombre_modulo) FROM Tarea where dni_alumno = '${dni}' AND nombre_modulo = '${nombre_modulo}'`;

    connection.query(consulta, function (err, rows, fields) {
        if (err) throw err;
        response.status(200).json(rows)
    });

}

app.post(baseUrl + '/getCount', getCount);

//-----------------------------------------------------------------------------------------------------------

//Obtenemos el nombre de los alumnos que realizan cierto modulo para rellenar las tablas

const getAlumnosModulo = (request, response) => {

    const {nombre_modulo}=request.body;

    var consulta = `SELECT DISTINCT Alumno.nombre_alumno FROM Tarea, Alumno WHERE Tarea.dni_alumno = Alumno.dni_alumno and Tarea.nombre_modulo = '${nombre_modulo}' ORDER BY Alumno.dni_alumno`;

    connection.query(consulta, function (err, rows, fields) {
        if (err) throw err;
        response.status(200).json(rows)
    });

}

app.post(baseUrl + '/getAlumnosModulo', getAlumnosModulo);

//-----------------------------------------------------------------------------------------------------------

// SET NOTAS --> Insertamos las notas

const setNotas = (request, response) => {

    console.log(request.body);

    var consulta = "INSERT INTO Tarea set ?";

    connection.query(consulta, [request.body], (err, rows)=>{

        if(err) return response.send(err);

        response.send("Añadido");

    });

}

app.post(baseUrl + '/setNotas', setNotas);

//-----------------------------------------------------------------------------------------------------------

// DELETE NOTAS

const deleteNotas = (request, response) => {

    console.log(request.params.nombre_entrega);
    //const {nombre_entrega}=request.params.nombre_entrega;

    var consulta = "DELETE FROM Tarea WHERE nombre_entrega = ? ";

    console.log(consulta)

    connection.query(consulta, [request.params.nombre_entrega], (err, rows)=>{

        console.log("Entro en la consulta");
        if(err) return response.send(err);
        response.send("Eliminado!");
    });
}

app.delete(baseUrl + '/getNotas/:nombre_entrega', deleteNotas);

//----------------------------------------------------------------------------

// GET DETAILED NOTAS

const getDetailedNotas = (request, response) => {

    const {dni}=request.body;

    var consulta = `SELECT * FROM Tarea where dni_alumno = '${dni}'`;
=======
    connection.connect();

    var consulta = "SELECT * FROM Alumno"
>>>>>>> 17ef2cea9b5ae55df4ed1135d854e22f5115164c

    connection.query(consulta, function (err, rows, fields) {
        if (err) throw err;
        response.status(200).json(rows)
    });

<<<<<<< HEAD
}

app.post(baseUrl + '/getDetailedNotas', getDetailedNotas);

//----------------------------------------------------------------------------

// UPDATE NOTAS

const updateNotas = (request, response) => {
    console.log(request.body);
    console.log(request.params.nombre_entrega);

    var consulta = "UPDATE Tarea set ? WHERE id_tarea = ?";

    connection.query(consulta, [request.body, request.params.nombre_entrega], (err, rows)=>{

        if(err) return response.send(err);
        response.send("Updated!");
    });
}

app.post(baseUrl + '/getNotas/:nombre_entrega', updateNotas);

//----------------------------------------------------------------------------

// GET LOGIN PROFE --> LOGIN PROFESOR

const getLoginProfesor = (request, response) => {

    console.log(request.body);
    const {dni,pass}=request.body;

    //Si el DNI y/o PASSWORD introducidos están vacíos, undefined o null gestiono el error
    if (dni == "" || dni == null || typeof dni === 'undefined' || pass == "" || pass == null || typeof dni === 'undefined'){
        response.status(401).json({
            message: "ERROR"
        })
    }

    var consulta = `SELECT pass_profesor FROM Profesor WHERE dni_profesor='${dni}'`;
    let user_pass;

    connection.query(consulta, function (err, rows, fields) {

        //Este if gestiona errores con la base de datos
        if (err){
            console.log("Se ha producido un error SQL");

        //Realiza la query en caso que no haya problemas con la base de datos
        }else{
            
            //Este if mira si la consulta SQL devuelve un undefined y muestra el error 401
            if(rows[0] == null){
                response.status(401).json({
                    message: "ERROR"
                })
            }

            //Si la consulta devuelve resultados se hace la comprovación de que el password recibido corresponda con el introducido
            else{
                user_pass=rows[0].pass_profesor;

                //Si el password recibido se corresponde con el introducido devuelve un mensaje: "OK" y un token de acceso
                if (user_pass == pass){
                    const user = {
                        nombre: dni
                    }
                    jwt.sign({user: user}, 'secretKey',{expiresIn: '32s'}, (err, token) => {
                        response.status(200).json({
                            message: "OK",
                            token
                        })
                    })

                //Si el password no se corresponde con el introducido devuelve un mensaje: "ERROR"
                }else{
                    response.status(401).json({
                        message: "ERROR"
                    })
                }
            }
        }
    });
}

app.post(baseUrl + '/LoginProfesor', getLoginProfesor);

//-----------------------------------------------------------------------------------------------------------

// GET LOGIN ALUMNO --> LOGIN ALUMNO

const getLoginAlumno = (request, response) => {
=======
    connection.end();
}

app.get(baseUrl + '/getAlumnos', getAlumnos);

const getLogin = (request, response) => {

    connection.connect();
>>>>>>> 17ef2cea9b5ae55df4ed1135d854e22f5115164c

    console.log(request.body);
    const {dni,pass}=request.body;

    //Si el DNI y/o PASSWORD introducidos están vacíos, undefined o null gestiono el error
    if (dni == "" || dni == null || typeof dni === 'undefined' || pass == "" || pass == null || typeof dni === 'undefined'){
        response.status(401).json({
            message: "ERROR"
        })
    }

    var consulta = `SELECT pass_alumno FROM Alumno WHERE dni_alumno='${dni}'`;
    let user_pass;

    connection.query(consulta, function (err, rows, fields) {

        //Este if gestiona errores con la base de datos
        if (err){
            console.log("Se ha producido un error SQL");

        //Realiza la query en caso que no haya problemas con la base de datos
        }else{
            
            //Este if mira si la consulta SQL devuelve un undefined y muestra el error 401
            if(rows[0] == null){
                response.status(401).json({
                    message: "ERROR"
                })
            }

            //Si la consulta devuelve resultados se hace la comprovación de que el password recibido corresponda con el introducido
            else{
                user_pass=rows[0].pass_alumno;

                //Si el password recibido se corresponde con el introducido devuelve un mensaje: "OK" y un token de acceso
                if (user_pass == pass){
                    const user = {
                        nombre: dni
                    }
                    jwt.sign({user: user}, 'secretKey',{expiresIn: '32s'}, (err, token) => {
                        response.status(200).json({
                            message: "OK",
                            token
                        })
                    })

                //Si el password no se corresponde con el introducido devuelve un mensaje: "ERROR"
                }else{
                    response.status(401).json({
                        message: "ERROR"
                    })
                }
            }
        }
    });
<<<<<<< HEAD
}

app.post(baseUrl + '/LoginAlumno', getLoginAlumno);

//-----------------------------------------------------------------------------------------------------------

// GET MODULOS --> Cargamos los modulos del profesor correspondiente a partir de su dni

const getModulos = (request, response) => {

    const {dni}=request.body;
=======
    //connection.end();
}

app.post(baseUrl + '/Login', getLogin);

const getModulos = (request, response) => {

    //connection.connect();

    const {dni}=request.body;
    console.log(request.body)
    console.log(dni);
>>>>>>> 17ef2cea9b5ae55df4ed1135d854e22f5115164c

    var consulta = `SELECT * FROM Modulo WHERE dni_professor = '${dni}'`;

    connection.query(consulta, function (err, rows, fields) {
        console.log(consulta);
        if (err) throw error;
        console.log(rows);
        response.status(200).json(rows)
    });

<<<<<<< HEAD
=======
    connection.end();
>>>>>>> 17ef2cea9b5ae55df4ed1135d854e22f5115164c
}

app.post(baseUrl + '/getModulos', getModulos);

<<<<<<< HEAD
//-----------------------------------------------------------------------------------------------------------

// GET MODULOS ALUMNO --> Cargamos los modulos del alumno correspondiente a partir de su dni

const getModulosAlumno = (request, response) => {

    const {dni}=request.body;

    var consulta = `SELECT nombre_modulo FROM AlumnoModulo WHERE dni_alumno = '${dni}'`;

    connection.query(consulta, function (err, rows, fields) {
        console.log(consulta);
        if (err) throw error;
        console.log(rows);
        response.status(200).json(rows)
    });

}

app.post(baseUrl + '/getModulosAlumno', getModulosAlumno);

//-----------------------------------------------------------------------------------------------------------

// Verify Token
=======
/*app.post(baseUrl+'/loginuser', (req, res) => {
    const user = {
        id:1,
        nombre: "Facu",
        email: "facu@email.com"
    }

    jwt.sign({user: user}, 'secretKey',{expiresIn: '32s'}, (err, token) => {
        res.json({
            token
        })
    })


});*/
>>>>>>> 17ef2cea9b5ae55df4ed1135d854e22f5115164c

app.post(baseUrl+'/posts',verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretKey', (err, authData)=> {
        if(err){
            res.sendStatus(403);
        }else{
            res.json({
                mensaje: "Post fue creado",
                authData
            });
        }
    })
});

//Authorization: Bearer <token>
function verifyToken(req,res,next){
    const bearerHeader = req.headers['authorization'];

    if (typeof bearerHeader !== 'undefined'){
        const bearerToken = bearerHeader.split(" ")[1];
        req.token = bearerToken;
        next();
    }else{
        res.sendStatus(403);
    }
}

<<<<<<< HEAD
//-----------------------------------------------------------------------------------------------------------

// GET NOTA ALUMNO --> Cargamos las tareas de un alumno en concreto cuya nota sea un 0
const getNotaAlumno = (request, response) => {

    const {dni}=request.body;
    console.log(request);

    var consulta = `SELECT nombre_entrega, nombre_modulo, uf FROM Tarea WHERE dni_alumno = '${dni}' and nota = 0;`;
    console.log(consulta);
    connection.query(consulta, function (err, rows, fields) {
        if (err) throw err;
        response.status(200).json(rows)
    });

}

app.post(baseUrl + '/getNotaAlumno', getNotaAlumno);

=======
>>>>>>> 17ef2cea9b5ae55df4ed1135d854e22f5115164c
//Inicialitzem el servei
const PORT = process.env.PORT || 3000; // Port
const IP = process.env.IP || null; // IP

app.listen(PORT, IP, () => {
    console.log("El servidor está inicialitzat en el puerto " + PORT);
});